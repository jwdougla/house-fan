#!/usr/bin/python

import time
from MPL115A2 import MPL115A2

class Attic():
        def __init__(self):
            self.mp = MPL115A2()
            self.mp.sleep()

        def getTemp(self):
            rList = self.mp.getPT()
            temp = rList[1]
            self.mp.sleep()
            return (int)(temp)
        def getPressure(self):
            rList= self.mp.getPT()
            p = rList[0]
            self.mp.sleep()
            return p


