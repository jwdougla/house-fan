from Adafruit_BMP085 import BMP085
import time
import math

class pWatch:
    cutoff = 30
    def __init__(self, sensor, control):
        self.sensor = sensor
        self.control = control
        self.shutoff = control.ShutOff
        self.ErrString = control.ErrString
        self.loop()

    def loop(self):
        #Initialize readings
        initial = sensor.readPressure()

        avg = initial

        while True:
            count = 0
            total = 0
            while count < 300:
                count = count + 1
                read = False
                while read == False:
                    pressure = sensor.readPressure()
                    if math.fabs(avg - pressure) < 150:
                        read = True

                total = total + pressure

                if (avg - pressure) > pWatch.cutoff:
                    self.shutoff = True
                    self.control.Stop()

                time.sleep(.1)

            avg = total/count

