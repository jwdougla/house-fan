import RPi.GPIO as GPIO
import time

class Relay():
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(27, GPIO.OUT)
        GPIO.setup(22, GPIO.OUT)
        GPIO.output(27, 0)
        GPIO.output(22, 0)
        self.state = False

    def on(self):
        GPIO.output(27, 1)
        GPIO.output(22, 1)
        self.state = True

    def off(self):
        GPIO.output(27, 0)
        GPIO.output(22, 0)
        self.state = False

    def getState(self):
        return self.state

    def cleanup(self):
        GPIO.cleanup()
