import urllib2
import json

def getTemp():
    weatherURL = "http://api.openweathermap.org/data/2.5/weather?lat=37.992481&lon=-121.7734&units=imperial"
    responce = urllib2.urlopen(weatherURL)
    html = responce.read()
    data = json.loads(html)
    return data['main']['temp']

