import urllib2
import json
from pprint import pprint
import time

class API():
    def __init__(self):
        self.getData()
    def getData( self ):
        f = urllib2.urlopen( \
        'http://api.wunderground.com/api/3382c492850195ba/geolookup/conditions/q/37.99244,-121.773279.json')
        json_string = f.read()
        self.data = json.loads(json_string)
        self.time = time.time()
        f.close()

    def getTemp( self):
        if (time.time() - self.time) > 600:
            self.getData()
            print "time refreshed"
        #print time.time() - self.time
        return self.data['current_observation']['temp_f']

    def printRaw(self):
        pprint(self.data)


if __name__ == "__main__":
    api = API()
    print api.getTemp()

