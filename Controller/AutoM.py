#!/usr/bin/python
import Tkinter
import tkFont

class Auto_Mode():
    itemp = 0
    otemp = 0

    def __init__(self, parent, control):
        self.root = parent.root
        self.parent = parent
        self.Control = control
        self.therm = self.Control.therm
        self.initialize()

    def initialize(self):
        self.Control.setMode(3)
        self.on = self.Control.getOn()

        bFont = tkFont.Font(family="Courier",size=33, weight="bold")
        subFont = tkFont.Font(family="Helvetica",size=32)
        TFont = tkFont.Font(family="Arial",size=85)
        TitleFont = tkFont.Font(family="Helvetica",size=48)

        self.mainFrame=Tkinter.Frame(self.root,cursor="none")
        self.mainFrame.pack()
        self.buttonFrame = Tkinter.Frame(self.mainFrame)
        self.buttonFrame.pack(side = Tkinter.LEFT)
        buttonBack = Tkinter.Button(
            self.buttonFrame,
            text="Back",
            bg="blue",
            font=bFont,
            height=2,
            width=6,
            command=self.closeApp, 
            activebackground="blue"
            )
        buttonBack.pack()

        self.buttonOnOff = Tkinter.Button(
            self.buttonFrame,
            text="Start",
            bg="green",
            font=bFont,
            height=2,
            width=6,
            command=self.startStop, 
            activebackground="green"
            )
        self.buttonOnOff.pack()

        self.buttonIncrement = Tkinter.Button(
            self.buttonFrame,
            text="Up",
            bg="red",
            font=bFont,
            height=2,
            width=6, 
            command=self.inc_target,
            activebackground="red"
            )
        self.buttonIncrement.pack()

        self.buttonDecrement = Tkinter.Button(
            self.buttonFrame,
            text="Down",
            bg="yellow",
            font=bFont,
            height=2,
            width=6, 
            command=self.dec_target,
            activebackground="yellow"
            )
        self.buttonDecrement.pack()

        self.canvas = Tkinter.Canvas(
            self.mainFrame, 
            width=528, 
            height=460,
            bg="white"
            )
        self.canvas.pack()

        self.canvas.create_text(264,50,text="Auto Mode",font=TitleFont)
        self.canvas.create_text(88,125,text="Inside",font=subFont)
        self.canvas.create_text(440,125,text="Outside",font=subFont)
        self.canvas.create_text(264,150,text="Target",font=subFont)
        self.TarTemp = self.canvas.create_text(
            264, 
            225,
            text=self.therm.getTarget(),
            font=TFont
            )

        self.canvas.create_text(132,300,text="Thermostat",font=subFont)
        self.thermL = self.canvas.create_text(
            132,
            375,
            text="OFF",
            fill="red",
            font=TFont
            )

        self.InTemp = self.canvas.create_text(
            88,
            200,
            text=self.Control.iTemp(),
            font=TFont
            )
        self.OutTemp= self.canvas.create_text(
            440,
            200,
            text=self.Control.oTemp(),
            font=TFont
            )


        self.canvas.create_text(396,300,text="Fan",font=subFont)
        self.OnOff = self.canvas.create_text(
            396,
            375,
            text="OFF",
            font=TFont,
            fill="red"
            )
        self.update_fan()
        self.update_temp()

    def update_temp(self):
        if self.mainFrame.winfo_exists():
            itemp = self.Control.iTemp()
            otemp = self.Control.oTemp()
            if(itemp != self.itemp):
                self.itemp = itemp
                self.canvas.itemconfig(self.InTemp, text=itemp)

            if(otemp != self.otemp):
                self.otemp = otemp
                self.canvas.itemconfig(self.OutTemp, text=otemp)

            self.root.after(1000, self.update_temp)

    def update_fan(self):
        if self.mainFrame.winfo_exists() == 0:
            return 0
        on = self.Control.getOn()
        if self.on != on:
            if on == False:
                self.on = False
                self.canvas.itemconfig(self.OnOff,text="OFF",fill="red")
            else:
                self.on = True
                self.canvas.itemconfig(self.OnOff, text="ON",fill="green")
        self.root.after(1000, self.update_fan)

    def closeApp(self):
        self.therm.stop()
        self.mainFrame.destroy()
        self.parent.initialize()

    def inc_target(self):
        self.therm.incTarget()
        self.canvas.itemconfig(self.TarTemp, text=self.therm.getTarget())

    def dec_target(self):
        self.therm.decTarget()
        self.canvas.itemconfig(self.TarTemp, text=self.therm.getTarget())

    def startStop(self):
        if self.therm.on == False:
            self.therm.start()
            self.buttonOnOff["text"]="Stop"
            self.canvas.itemconfig(self.thermL,text="ON",fill="green")
        else:
            self.therm.stop()
            self.buttonOnOff["text"]="Start"
            self.canvas.itemconfig(self.thermL,text="OFF",fill="red")

