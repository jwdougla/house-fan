#!/usr/bin/python
import datetime
import Tkinter
import tkFont
import Timer
import TimeM
import ManualM
import AutoM
import OptionsM

class Menu(Tkinter.Tk):
    itemp = 0
    otemp = 0
    atemp = 0

    def __init__(self, parent, control):
        self.root = Tkinter.Tk()
        self.parent = parent
        self.Control=control
        self.initialize()
        self.root.mainloop()

    def initialize(self):
        self.Control.setMode(0)
        self.on = False
        bFont = tkFont.Font(family="Courier", size=33, weight="bold")
        subFont = tkFont.Font(family="Helvetica", size=32)
        TFont = tkFont.Font(family="Arial", size=85)
        TitleFont = tkFont.Font(family="Helvetica", size=48)

        self.root.overrideredirect(True)
        self.root.geometry(
            "{0}x{1}+0+0".format(
                self.root.winfo_screenwidth(), 
                self.root.winfo_screenheight()
                )
            )

        self.mainFrame = Tkinter.Frame(self.root, cursor="none")
        self.mainFrame.pack()
        self.buttonFrame = Tkinter.Frame(self.mainFrame)
        self.buttonFrame.pack(side=Tkinter.LEFT)
        buttonManual = Tkinter.Button(
            self.buttonFrame,
            text="Manual",
            bg="blue",
            font=bFont,
            height=2,
            width=6,
            command=self.start_Manual,
            activebackground="blue"
            )
        buttonManual.pack()

        self.buttonTime = Tkinter.Button(
            self.buttonFrame,
            text="Time",
            bg="green",
            font=bFont,
            height=2,
            width=6,
            command=self.start_Timer,
            activebackground="green"
            )
        self.buttonTime.pack()

        self.buttonAuto = Tkinter.Button(
            self.buttonFrame,
            text="Auto",
            bg="red",
            font=bFont,
            height=2,
            width=6, 
            command=self.start_Auto,
            activebackground = "red"
            )
        self.buttonAuto.pack()

        self.buttonOptions = Tkinter.Button(
            self.buttonFrame,
            text="Options",
            bg="yellow",
            font=bFont,
            height=2,
            width=6, 
            command=self.start_Options, 
            activebackground="yellow"
            )
        self.buttonOptions.pack()

        self.canvas = Tkinter.Canvas(
            self.mainFrame, 
            width=528, 
            height=460,
            bg="white"
            )
        self.canvas.pack(side = Tkinter.LEFT)

        self.canvas.create_text(264,50, text="Menu", font=TitleFont)
        self.canvas.create_text(88,150, text="Inside", font=subFont)
        self.canvas.create_text(440,150, text="Outside", font=subFont)
        self.tLabel = self.canvas.create_text(
            265,
            150,
            text="Attic",
            font=subFont
            )

        self.InTemp = self.canvas.create_text(
            88,
            250,
            text=self.Control.iTemp(),
            font=TFont
            )
        self.OutTemp= self.canvas.create_text(
            440,
            250,
            text=self.Control.oTemp(),
            font=TFont
            )
        self.AttTemp= self.canvas.create_text(
            264,
            250,
            text=self.Control.aTemp(),
            font=TFont
            )
        self.canvas.create_text(
            500,
            440,
            text=datetime.datetime.now().strftime("%H:%M")
            )
        self.update()

    def update(self):
        if self.mainFrame.winfo_exists():
            itemp = self.Control.iTemp()
            otemp = self.Control.oTemp()
            atemp = self.Control.aTemp()
            if(itemp != self.itemp):
                self.itemp = itemp
                self.canvas.itemconfig(self.InTemp, text=itemp)

            if(otemp != self.otemp):
                self.otemp = otemp
                self.canvas.itemconfig(self.OutTemp, text=otemp)

            if(atemp != self.atemp):
                self.atemp = atemp
                self.canvas.itemconfig(self.AttTemp, text=self.Control.aTemp())

            self.root.after(1000, self.update)

    def start_Manual(self):
        self.mainFrame.destroy()
        ManualM.Manual_Mode(self, self.Control)

    def start_Timer(self):
        self.mainFrame.destroy()
        TimeM.Timer_Mode(self, self.Control)

    def start_Auto(self):
        self.mainFrame.destroy()
        AutoM.Auto_Mode(self, self.Control)

    def start_Options(self):
        self.mainFrame.destroy()
        OptionsM.Options_Mode(self, self.Control)
