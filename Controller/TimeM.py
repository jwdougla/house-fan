#!/usr/bin/python

import Tkinter
import tkFont
import Timer

class Timer_Mode():
    itemp = 0
    otemp = 0

    def __init__(self, parent, control):
        self.root = parent.root
        self.parent = parent
        self.Control = control
        self.initialize()

    def initialize(self):
        self.Control.setMode(2)
        self.on = self.Control.getOn()
        bFont = tkFont.Font(family="Courier",size=33, weight="bold")
        subFont = tkFont.Font(family="Helvetica",size=32)
        TFont = tkFont.Font(family="Arial",size="85")
        TitleFont=tkFont.Font(family="Helvetica",size=48)

        self.timer = self.Control.timer

        self.mainFrame=Tkinter.Frame(self.root,cursor="none")
        self.mainFrame.pack()
        self.buttonFrame = Tkinter.Frame(self.mainFrame)
        self.buttonFrame.pack(side = Tkinter.LEFT)
        buttonBack = Tkinter.Button(
            self.buttonFrame,
            text="Back",
            bg="blue",
            font=bFont,
            height=2,
            width=6,
            command=self.closeApp, 
            activebackground="blue"
            )
        buttonBack.pack()

        self.buttonOnOff = Tkinter.Button(
            self.buttonFrame,
            text="Start",
            bg="green",
            font=bFont,
            height=2,
            width=6,
            command=self.start_clock, 
            activebackground="green"
            )
        self.buttonOnOff.pack()

        self.buttonIncrement = Tkinter.Button(
            self.buttonFrame,
            text="More",
            bg="red",
            font=bFont,
            height=2,
            width=6, 
            command=self.inc_clock, 
            activebackground="red"
            )
        self.buttonIncrement.pack()

        self.buttonDecrement = Tkinter.Button(
            self.buttonFrame,
            text="Less",
            bg="yellow",
            font=bFont,
            height=2,
            width=6, 
            command=self.dec_clock, 
            activebackground="yellow"
            )
        self.buttonDecrement.pack()

        self.canvas = Tkinter.Canvas(
            self.mainFrame, 
            width=528, 
            height=460,
            bg="white"
            )
        self.canvas.pack()

        self.canvas.create_text(264,50,text="Timed Mode",font=TitleFont)
        self.canvas.create_text(88,125,text="Inside",font=subFont)
        self.canvas.create_text(440,125,text="Outside",font=subFont)
        self.tLabel = self.canvas.create_text(
            264,
            275,
            text="Time",
            font=subFont
            )

        self.InTemp = self.canvas.create_text(
            88,
            200,
            text=self.Control.iTemp(),
            font=TFont
            )
        self.OutTemp= self.canvas.create_text(
            440,
            200,
            text=self.Control.oTemp(),
            font=TFont
            )

        hours = self.timer.getHours()
        minutes=self.timer.getMinutes()
        sec = self.timer.getSeconds()
        self.clock = self.canvas.create_text(
            264,
            350,
            text=("%02d:%02d:%02d" % (hours, minutes,sec)),
            font=TFont
            )
        self.OnOff = self.canvas.create_text(
            264,
            150,
            text="OFF",
            font="Arial 70",
            fill="red"
            )
        self.update_clock()
        self.update_temp()

    def update_temp(self):
        if self.mainFrame.winfo_exists():
            itemp = self.Control.iTemp()
            otemp = self.Control.oTemp()
            if(itemp != self.itemp):
                self.itemp = itemp
                self.canvas.itemconfig(self.InTemp, text=itemp)

            if(otemp != self.otemp):
                self.otemp = otemp
                self.canvas.itemconfig(self.OutTemp, text=otemp)

            self.root.after(1000, self.update_temp)

    def update_clock(self):
        if self.mainFrame.winfo_exists() == 0:
            return 0
        hours = self.timer.getHours()
        minutes=self.timer.getMinutes()
        sec = self.timer.getSeconds()
        self.canvas.itemconfig(
            self.clock, 
            text=("%02d:%02d:%02d" %(hours,minutes,sec))
            )
        on = self.Control.getOn()
        if self.on != on:
            if on == False:
                self.on = False
                self.buttonOnOff["text"]="Start"
                self.canvas.itemconfig(self.OnOff,text="OFF",fill="red")
                self.canvas.itemconfig(self.tLabel,text="Time")
                self.buttonIncrement["state"]="normal"
                self.buttonDecrement["state"]="normal"
                self.buttonIncrement["bg"]="red"
                self.buttonDecrement["bg"]="yellow"
            else:
                self.on = True
                self.canvas.itemconfig(self.OnOff, text="ON",fill="green")
                self.canvas.itemconfig(self.tLabel,text="Remaining")
                self.buttonOnOff["text"]="Stop"
                self.buttonIncrement["state"]="disabled"
                self.buttonIncrement["bg"]="gray"
                self.buttonDecrement["state"]="disabled"
                self.buttonDecrement["bg"]="gray"
        self.root.after(1000, self.update_clock)

    def closeApp(self):
        self.timer.reset()
        self.mainFrame.destroy()
        self.parent.initialize()

    def inc_clock(self):
        self.timer.add()
        hours = self.timer.getHours()
        minutes=self.timer.getMinutes()
        sec = self.timer.getSeconds()
        self.canvas.itemconfig(
            self.clock, 
            text=("%02d:%02d:%02d" %(hours,minutes,sec))
            )

    def dec_clock(self):
        self.timer.sub()
        hours = self.timer.getHours()
        minutes=self.timer.getMinutes()
        sec = self.timer.getSeconds()
        self.canvas.itemconfig(
            self.clock, 
            text=("%02d:%02d:%02d" %(hours,minutes,sec))
            )


    def start_clock(self):
        if self.timer.getOn() == False:
            self.timer.start()
            """self.canvas.itemconfig(self.OnOff, text="ON",fill="green")
            self.canvas.itemconfig(self.tLabel,text="Remaining")
            self.buttonOnOff["text"]="Stop"
            self.buttonIncrement["state"]="disabled"
            self.buttonIncrement["bg"]="gray"
            self.buttonDecrement["state"]="disabled"
            self.buttonDecrement["bg"]="gray"""
        else:
            self.timer.reset()
            #self.on = False
            #self.buttonOnOff["text"]="Start"
            #self.canvas.itemconfig(self.tLabel,text="Time")
            #self.canvas.itemconfig(self.OnOff,text="OFF",fill="red")
            #self.buttonIncrement["state"]="normal"
            #self.buttonDecrement["state"]="normal"
            #self.buttonIncrement["bg"]="red"
            #self.buttonDecrement["bg"]="yellow"


if __name__ == "__main__":
    app = Timer_Mode(None)
    app.title('Timer Mode')
    app.mainloop()
