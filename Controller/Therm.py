#!/usr/bin/python

import time

class Therm:
    default = 66
    def __init__(self, controller):
        self.target = Therm.default
        self.control = controller
        self.on = False

    def incTarget(self):
        self.target = self.target + 1
        return True

    def decTarget(self):
        self.target = self.target - 1
        return True

    def start(self):
        self.on = True

    def stop(self):
        self.on = False

    def fan(self):
        if self.on == False:
            return False
        iTemp = self.control.iTemp()
        oTemp = self.control.oTemp()
        if iTemp < self.target:
            return False

        if oTemp < iTemp:
            return True

    def reset(self):
        self.target = Therm.default
        self.on = False

    def getTarget(self):
        return self.target
