from Adafruit_BMP085 import BMP085 as BMP
import ErrorMessage
import thread
import Menu
import time
import wgAPI
import MPL
import Relay
import Timer
import Therm

class Controller:
    def __init__(self):

        self.running = True
        self.mode = 0
        self.ShutOff = False

        try:
            self.bmp = BMP(0x77, 3)
        except:
            ErrorMessage.error("Inside Sensor Failure")

        try:
            self.att = MPL.Attic()
        except:
            ErrorMessage.error("Attic Sensor Failure")

        try:
            self.weather = wgAPI.API()
        except:
            ErrorMessage.error("WiFi/Weather Failure")

        #Add attemp to get serial data

        self.relay = Relay.Relay()
        self.timer = Timer.Timer()
        self.therm = Therm.Therm(self)

        #thread.start_new_thread(Failsafe.pWatch() ,(self.bmp,self,))


    def iTemp(self):
        try:
            temp = self.bmp.readTemperature()
            temp = (int)(temp*9/5+32)
        except:
            ErrorMessage.error("Failed Inside Temp Sensor")
            temp = 0
        return temp

    def oTemp(self):
        #Try to get from weather station
        #if fails
        try:
            temp = self.weather.getTemp()
            temp = (int)(temp)
        except:
            ErrorMessage.error("WiFi/Weather Failure")
            temp = 0
        return temp

    def aTemp(self):
        try:
            temp = self.att.getTemp()
            temp = (int)(temp*9/5+39)
        except:
            ErrorMessage.error("Failed Attic Sensor")
            temp = 0
        return temp

    def setMode(self, imod):
        self.mode = imod

    def getOn(self):
        return self.relay.getState()

    def Start(self):
        if self.ShutOff:
            return False
        self.relay.on()
        self.on = True
        return True

    def Stop(self):
        self.relay.off()
        self.on = False
        return self.relay.getState()

    def loop(self):
        while self.running:
            if self.mode == 1:  #Manual Mode
                time.sleep(.25)
            elif self.mode == 2:    #Timer Mode
                onOff = self.timer.getOn()
                if self.getOn != onOff:
                    if onOff:
                        self.Start()
                    else:
                        self.Stop()
                time.sleep(.1)
            elif self.mode == 3:    #Auto Mode
                onOff = self.therm.fan()
                if self.getOn() != onOff:
                    if onOff:
                        self.Start()
                    else:
                        self.Stop()
                time.sleep(1)
            else:
                if self.getOn() == True:
                    self.Stop()
                time.sleep(.25)
def GUI(control,stupid):
    stupid = 1
    app = Menu.Menu(None,control)

if __name__ == "__main__":
    Control = Controller()
    try:
        stupid = 0
        thread.start_new_thread(GUI,(Control,stupid , ))
        Control.loop()
    finally:
        Control.relay.cleanup()
