#!/usr/bin/python

import Tkinter
import tkFont

class Options_Mode():
    itemp = 0
    otemp = 0
    atemp = 0

    def __init__(self, parent, control):
        self.root = parent.root
        self.parent = parent
        self.Control = control
        self.initialize()

    def initialize(self):
        self.Control.setMode(1)
        self.on = False

        bFont = tkFont.Font(family="Courier",size=33, weight="bold")
        subFont = tkFont.Font(family="Helvetica",size=32)
        TFont=tkFont.Font(family="Arial",size=85)
        TitleFont = tkFont.Font(family="Helvetica",size=48)

        self.mainFrame=Tkinter.Frame(self.root,cursor="none")
        self.mainFrame.pack()
        self.buttonFrame = Tkinter.Frame(self.mainFrame)
        self.buttonFrame.pack(side = Tkinter.LEFT)

        buttonBack = Tkinter.Button(
            self.buttonFrame,
            text="Back",
            bg="blue",
            font=bFont,
            height=2,
            width=6,
            command=self.closeApp,
            activebackground="blue"
            )
        buttonBack.pack()

        self.buttonOnOff = Tkinter.Button(
            self.buttonFrame,
            text="Reset Defaults",
            bg="green",font=bFont,
            height=2,
            width=6,
            command=self.default,
            activebackground="green"
            )
        self.buttonOnOff.pack()

        self.buttonIncrement = Tkinter.Button(
            self.buttonFrame,
            text="Reset Killswitch",
            bg="red",
            font=bFont,
            height=2,
            width=6,
            state="disabled"
            )
        self.buttonIncrement.pack()

        self.buttonDecrement = Tkinter.Button(
            self.buttonFrame,text="Exit",
            bg="yellow",font=bFont,
            height=2,
            width=6,
            command=self.exitProgram
            )
        self.buttonDecrement.pack()

        self.canvas = Tkinter.Canvas(
            self.mainFrame,
            width=528,
            height=460,
            bg="white"
            )
        self.canvas.pack()

        self.canvas.create_text(264,50,text="Options Mode",font=TitleFont)
        self.canvas.create_text(88,150,text="Inside",font=subFont)
        self.canvas.create_text(440,150,text="Outside",font=subFont)
        self.canvas.create_text(265,250,text="Attic",font=subFont)

        self.InTemp= self.canvas.create_text(
            88,
            225,
            text=self.Control.iTemp(),
            font=TFont
            )
        self.AttTemp= self.canvas.create_text(
            264,
            325,
            text=self.Control.aTemp(),
            font=TFont
            )
        self.OutTemp= self.canvas.create_text(
            440,
            225,
            text=self.Control.oTemp(),
            font=TFont
            )

        self.OnOff = self.canvas.create_text(264,
            150,
            text="OFF",
            font="Arial 75",
            fill="red"
            )
        self.update_temp()

    def update_temp(self):
        if self.mainFrame.winfo_exists():
            itemp = self.Control.iTemp()
            otemp = self.Control.oTemp()
            atemp = self.Control.aTemp()
            if(itemp != self.itemp):
                self.itemp = itemp
                self.canvas.itemconfig(self.InTemp, text=itemp)

            if(otemp != self.otemp):
                self.otemp = otemp
                self.canvas.itemconfig(self.OutTemp, text=otemp)

            if(atemp != self.atemp):
                self.atemp = atemp
                self.canvas.itemconfig(self.AttTemp,text=self.Control.aTemp())

            self.root.after(1000, self.update_temp)

    def closeApp(self):
        self.Control.Stop()
        self.mainFrame.destroy()
        self.parent.initialize()

    def default(self):
        self.Control.therm.reset()

    def exitProgram(self):
        self.Control.Stop()
        self.Control.running = False
        self.root.destroy()
        exit()

    def start_fan(self):
        if self.Control.getOn() == False:
            if self.Control.Start() == True:
                self.canvas.itemconfig(self.OnOff, text="ON",fill="green")
                self.buttonOnOff["text"]="Stop"
        else:
            if self.Control.Stop() == False:
                self.buttonOnOff["text"]="Start"
                self.canvas.itemconfig(self.OnOff,text="OFF",fill="red")
