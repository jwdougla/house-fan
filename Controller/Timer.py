import thread
import time

class Timer:
    default = .25*60
    increment = 1

    def __init__(self):
        self.default = Timer.default
        self.time = self.default
        self.on = False
    def getHours(self):
        hours = (int)(self.getTime()/60)
        return hours

    def getMinutes(self):
        minutes = (int)(self.getTime()%60)
        return minutes

    def getSeconds(self):
        sec = (int)((self.getTime()*60)%60)
        return sec

    def start(self):
        self.startTime = time.time()
        self.on = True
        timeLength=self.time

    def reset(self):
        self.time = self.default
        self.on=False
        self.startTime=0

    def add(self):
        if self.on == False:
            self.time = self.time + Timer.increment

    def sub(self):
        if self.on == False and self.time > Timer.increment:
            self.time = self.time - Timer.increment

    def getTime(self):
        if self.on == True:
            currentTime = time.time()
            delta = float(currentTime - self.startTime)/60
            t= float(self.time-delta)
            if t > 0:
                return t
            else:
                self.reset()
                return self.time
        else:
            return self.time
    def getOn(self):
        self.getTime()
        return self.on





#countdown = Timer()
#countdown.start()
#while 1:
#   print "%02d:%02d:%02d" %(countdown.getHours(), countdown.getMinutes(), #countdown.getSeconds())
#   time.sleep(1)
